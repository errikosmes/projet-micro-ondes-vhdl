-- librairies
library IEEE;
use IEEE.std_logic_1164.all;

-- entité du controlleur
entity ctrl_oven is
    port(
        clk, rst, half_power, full_power, start, s30, s60, s120, time_set, door_open, timeout : in std_logic;
        full, half, in_light, finished, start_count, stop_count : out std_logic
    );
end entity;

architecture impl of ctrl_oven is
    -- états de l'automate
    type states_t is (idle, half_power_on, full_power_on, set_time, operation_enabled,
                      operation_disabled, operating, complete);
    signal state, next_state: states_t;

begin
    -- Calcul de l'état suivant
    process(state, half_power, full_power, start, s30, s60, s120, time_set, door_open, timeout)
    begin
        case state is
            when idle =>
                if full_power='1' then next_state <= full_power_on;
                elsif half_power='1' then next_state <= half_power_on;
                else next_state <= idle;
                end if;
            when full_power_on =>
                if half_power='1' then next_state <= half_power_on;
                elsif s30='1' or s60='1' or s120='1' then next_state <= set_time;
                else next_state <= full_power_on;
                end if;
            when half_power_on =>
                if full_power='1' then next_state <= full_power_on;
                elsif s30='1' or s60='1' or s120='1' then next_state <= set_time;
                else next_state <= half_power_on;
                end if;
            when set_time =>
                if time_set='1' and door_open='0' then next_state <= operation_enabled;
                elsif time_set='1' and door_open='1' then next_state <= operation_disabled;
                else next_state <= set_time;
                end if;
            when operation_enabled =>
                if door_open='1' then next_state <= operation_disabled;
                elsif start='1' then next_state <= operating;
                else next_state <= operation_enabled;
                end if;
            when operation_disabled =>
                if door_open='0' then next_state <= operation_enabled;
                else next_state <= operation_disabled;
                end if;
            when operating =>
                if door_open='1' then next_state <= operation_disabled;
                elsif timeout='1' then next_state <= complete;
                else next_state <= operating;
                end if;
            when complete =>
                if door_open='1' then next_state <= idle;
                else next_state <= complete;
                end if;
        end case; 
    end process;

    -- Mise à jour de l'état
    process(rst, clk)
    begin
        if rst='1' then state <= idle; --si reset asynchrone (actif haut)
        elsif clk'event and clk='1' then state <= next_state; --sur front montant
        end if;
    end process;

    -- Mise à jour de la sortie
    process(state, half_power, full_power, start, s30, s60, s120, time_set, door_open, timeout)
    begin
        -- TODO verify this
        -- why not
        -- if(state=full_power_on) then 
        --     full<='1';
        --     half<='0';
        -- elsif(state=half_power_on) then
        --     full<='0';
        --     half<='1'; 
        -- else 
        --     full<='0';
        --     half<='0';
        -- end if;

        if ((state=idle and full_power='1')
            or (state=half_power_on and full_power='1')
            or (state=full_power_on and not(s30='1' or s60='1' or s120='1'))
			) then full<='1';
        else full<='0';
        end if;

		if ((state=idle and half_power='1')
            or (state=full_power_on and half_power='1')
            or (state=half_power_on and not(s30='1' or s60='1' or s120='1'))
			) then half<='1';
        else half<='0';
        end if;

		if ((state=operation_enabled and start='1')
			or (state=operating and timeout='0')
			or door_open='1'
			) then in_light<='1';
        else in_light<='0';
        end if;

		if ((state=operating and timeout='1')
			or (state=complete and door_open='0')
			) then finished<='1';
        else finished<='0';
        end if;

		if (state=operation_enabled and start='1') then start_count<='1';
        else start_count<='0';
        end if;

		if (state=operating and door_open='1') then stop_count<='1';
        else stop_count<='0';
        end if;
		
	end process;

end architecture;
