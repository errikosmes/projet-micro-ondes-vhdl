library IEEE;
use IEEE.std_logic_1164.all;

entity counter_oven is
	port(
		clk, rst, start, stop, s30, s60, s120: in std_logic;
		aboveth: out std_logic
	);
end entity;

architecture impl of counter_oven is

	type States is (idle, counting, stopped);
	
	signal state, nextstate: States;
	signal c, nextc: natural; --natural number, 0 to Integer'high

begin

	process (state, c, start, stop, s30, s60, s120)
	begin
		case state is
			when idle =>
				if start='1' then
					nextstate <= counting;
					nextc <= c+1; --fill here c+1
				else
					nextstate <= idle;
					nextc <= 0; -- fill here 0
				end if;
			when counting =>
				if stop = '1' then
					nextstate <= stopped;
					nextc <= c; --fill here c
				elsif ((s30='1' and c >= 30) or (s60='1' and c >= 60) or (s120='1' and c >= 120)) then
					nextstate <= idle;
					nextc <= 0; --fill here 0
				else
					nextstate <= counting;
					nextc <= c+1; --fill here c+1
				end if;
			when stopped =>
				if start='1' then
					nextstate <= counting;
					nextc <= c+1; --fill here c+1
				else nextstate <= stopped;
					nextc <= c; --fill here c
				end if;
		end case;
	end process;

	process (rst, clk)
	begin
		-- if asynchronous reset
		if (rst = '1') then
			state <= idle;
			c <= 0; --fill here 0
		-- if rising edge
		elsif (clk'event and clk='1') then
			state <= nextstate;
			c <= nextc; --fill here nextc
		end if;
	end process;

	aboveth <= '1' when ((s30='1' and c >= 30) or (s60='1' and c >= 60) or (s120='1' and c >= 120)) else '0';

end architecture;
