-- Banc de test pour le contrôleur.
-- Chaque composant dispose d'une seule architecture, il n'est pas nécessaire
-- de spécifier une configuration particulière pour ce banc de test.

library IEEE;
use		IEEE.std_logic_1164.all;

library LIB_TP4;
use		LIB_TP4.ctrl_oven;
use		LIB_TP4.counter_oven;


-- Entité de test

entity bench_ctrl is
end entity;


architecture test of bench_ctrl is

	-- Composants utilisés
	component ctrl_oven is
		port(
			clk, rst, half_power, full_power, start, s30, s60, s120, time_set, door_open, timeout : in std_logic;
			full, half, in_light, finished, start_count, stop_count : out std_logic
		);
	end component;

	-- Signaux
	signal clk_sig         : std_logic := '0';
	signal rst_sig         : std_logic := '0';
	signal door_open_sig   : std_logic := '0';
	signal s30_sig         : std_logic := '0';
	signal s60_sig         : std_logic := '0';
	signal s120_sig        : std_logic := '0';
	signal half_power_sig  : std_logic := '0';
	signal full_power_sig  : std_logic := '0';
	signal start_sig       : std_logic := '0';
	signal time_set_sig    : std_logic := '0';
	signal full_sig        : std_logic := '0';
	signal half_sig        : std_logic := '0';
	signal in_light_sig    : std_logic := '0';
	signal finished_sig    : std_logic := '0';
	signal start_count_sig : std_logic := '0';
	signal stop_count_sig  : std_logic := '0';
	signal timeout_sig     : std_logic := '0';

-- Début de l'architecture
begin

	ctrl : ctrl_oven port map(
		clk => clk_sig,
		rst => rst_sig,
		half_power => half_power_sig,
		full_power => full_power_sig,
		start => start_sig,
		s30 => s30_sig,
		s60 => s60_sig,
		s120 => s120_sig,
		time_set => time_set_sig,
		door_open => door_open_sig,
		timeout => timeout_sig,
		full => full_sig,
		half => half_sig,
		in_light => in_light_sig,
		finished => finished_sig,
		start_count => start_count_sig,
		stop_count => stop_count_sig
	);

	-- Génération des stimulis
	clk_sig <= not clk_sig after 50 ns;
	rst_sig <= '1' after 2620 ns, '0' after 2720 ns,
		'1' after 2820 ns, '0' after 2920 ns;

	full_power_sig <= '1' after 120 ns, '0' after 320 ns, '1' after 2020 ns;
	half_power_sig <= '1' after 320 ns, '0' after 2020 ns;
	s30_sig <= '1' after 520 ns, '0' after 1900 ns;
	s60_sig <= '1' after 2220 ns;
	time_set_sig <= '1' after 720 ns;
	door_open_sig <= '1' after 730 ns, '0' after 920 ns, '1' after 1320 ns, '0' after 1420 ns, '1' after 1820 ns, '0' after 2320 ns, '1' after 2420 ns, '0' after 2520 ns;
	start_sig <= '1' after 1120 ns;
	timeout_sig <= '1' after 1620 ns;

end architecture;
