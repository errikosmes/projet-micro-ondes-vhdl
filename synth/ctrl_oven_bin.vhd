
-- 
-- Definition of  ctrl_oven_bin
-- 
--      Tue 08 Dec 2020 03:47:57 PM CET
--      
--      LeonardoSpectrum Level 3, 2014a.12
-- 

library c35_CORELIB;
use 	c35_CORELIB.vcomponents.all;

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity ctrl_oven_bin is
   port (
      clk : IN std_logic ;
      rst : IN std_logic ;
      half_power : IN std_logic ;
      full_power : IN std_logic ;
      start : IN std_logic ;
      s30 : IN std_logic ;
      s60 : IN std_logic ;
      s120 : IN std_logic ;
      time_set : IN std_logic ;
      door_open : IN std_logic ;
      timeout : IN std_logic ;
      full : OUT std_logic ;
      half : OUT std_logic ;
      in_light : OUT std_logic ;
      finished : OUT std_logic ;
      start_count : OUT std_logic ;
      stop_count : OUT std_logic) ;
end ctrl_oven_bin ;

architecture impl of ctrl_oven_bin is
   signal state_2, nx2, state_1, nx6, state_0, nx446, nx447, nx448, nx40, 
      nx50, nx64, nx84, nx104, nx112, nx122, nx134, nx144, nx456, nx461, 
      nx466, nx468, nx470, nx473, nx475, nx479, nx481, nx484, nx490, nx494, 
      nx496, nx498, nx500, nx503, nx508, nx511, nx515, nx517, nx521, nx523, 
      nx525, nx528: std_logic ;

begin
   ix149 : NOR40 port map ( Q=>stop_count, A=>nx456, B=>state_0, C=>nx470, D
      =>nx461);
   ix457 : CLKIN1 port map ( Q=>nx456, A=>door_open);
   ix65 : OAI211 port map ( Q=>nx64, A=>full_power, B=>nx446, C=>nx496);
   reg_state_0 : DFC1 port map ( Q=>state_0, QN=>nx466, C=>clk, D=>nx64, RN
      =>nx468);
   ix469 : CLKIN1 port map ( Q=>nx468, A=>rst);
   ix113 : OAI211 port map ( Q=>nx112, A=>nx473, B=>nx446, C=>nx475);
   ix474 : NOR31 port map ( Q=>nx473, A=>s60, B=>s120, C=>s30);
   ix476 : AOI221 port map ( Q=>nx475, A=>nx2, B=>nx84, C=>nx481, D=>nx104);
   ix3 : NOR21 port map ( Q=>nx2, A=>door_open, B=>nx461);
   ix85 : OAI211 port map ( Q=>nx84, A=>nx479, B=>state_0, C=>nx470);
   ix480 : CLKIN1 port map ( Q=>nx479, A=>start);
   ix482 : NOR21 port map ( Q=>nx481, A=>nx134, B=>nx2);
   ix135 : IMUX21 port map ( Q=>nx134, A=>nx484, B=>nx461, S=>nx122);
   ix485 : NAND21 port map ( Q=>nx484, A=>time_set, B=>nx461);
   ix123 : NAND21 port map ( Q=>nx122, A=>state_0, B=>state_1);
   reg_state_1 : DFC1 port map ( Q=>state_1, QN=>nx470, C=>clk, D=>nx112, RN
      =>nx468);
   reg_state_2 : DFC1 port map ( Q=>state_2, QN=>nx461, C=>clk, D=>nx447, RN
      =>nx468);
   ix491 : CLKIN1 port map ( Q=>nx490, A=>full_power);
   ix497 : MUX22 port map ( Q=>nx496, A=>nx498, B=>nx503, S=>nx481);
   ix499 : AOI2111 port map ( Q=>nx498, A=>timeout, B=>nx500, C=>nx50, D=>
      door_open);
   ix501 : NOR21 port map ( Q=>nx500, A=>state_0, B=>nx470);
   ix51 : NOR31 port map ( Q=>nx50, A=>nx466, B=>nx470, C=>nx461);
   ix504 : AOI211 port map ( Q=>nx503, A=>nx6, B=>nx500, C=>nx40);
   ix7 : CLKIN1 port map ( Q=>nx6, A=>nx473);
   ix41 : OAI211 port map ( Q=>nx40, A=>nx448, B=>nx470, C=>nx508);
   ix73 : NAND21 port map ( Q=>nx448, A=>state_0, B=>nx461);
   ix509 : OAI2111 port map ( Q=>nx508, A=>state_1, B=>nx490, C=>nx461, D=>
      half_power);
   ix159 : OAI221 port map ( Q=>finished, A=>nx511, B=>nx144, C=>nx122, D=>
      nx494);
   ix512 : CLKIN1 port map ( Q=>nx511, A=>timeout);
   ix145 : NAND21 port map ( Q=>nx144, A=>nx500, B=>state_2);
   ix175 : OAI2111 port map ( Q=>in_light, A=>timeout, B=>nx144, C=>nx515, D
      =>nx456);
   ix516 : NAND31 port map ( Q=>nx515, A=>nx517, B=>start, C=>state_2);
   ix518 : NOR21 port map ( Q=>nx517, A=>state_0, B=>state_1);
   ix195 : OAI311 port map ( Q=>half, A=>state_0, B=>nx521, C=>state_2, D=>
      nx523);
   ix522 : CLKIN1 port map ( Q=>nx521, A=>half_power);
   ix524 : NAND21 port map ( Q=>nx523, A=>nx473, B=>nx525);
   ix526 : NOR21 port map ( Q=>nx525, A=>nx448, B=>state_1);
   ix219 : OAI311 port map ( Q=>full, A=>nx490, B=>state_2, C=>state_1, D=>
      nx528);
   ix529 : NAND31 port map ( Q=>nx528, A=>nx500, B=>nx461, C=>nx473);
   ix137 : CLKIN1 port map ( Q=>nx447, A=>nx481);
   ix75 : CLKIN1 port map ( Q=>nx446, A=>nx525);
   ix495 : CLKIN1 port map ( Q=>nx494, A=>nx2);
   ix165 : CLKIN1 port map ( Q=>start_count, A=>nx515);
   ix105 : IMUX30 port map ( Q=>nx104, A=>state_2, B=>half_power, C=>nx490, 
      S0=>nx466, S1=>nx470);
end impl ;

