
-- 
-- Definition of  ctrl_oven_1hot
-- 
--      Tue 08 Dec 2020 04:10:22 PM CET
--      
--      LeonardoSpectrum Level 3, 2014a.12
-- 

library c35_CORELIB;
use 	c35_CORELIB.vcomponents.all;

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity ctrl_oven_1hot is
   port (
      clk : IN std_logic ;
      rst : IN std_logic ;
      half_power : IN std_logic ;
      full_power : IN std_logic ;
      start : IN std_logic ;
      s30 : IN std_logic ;
      s60 : IN std_logic ;
      s120 : IN std_logic ;
      time_set : IN std_logic ;
      door_open : IN std_logic ;
      timeout : IN std_logic ;
      full : OUT std_logic ;
      half : OUT std_logic ;
      in_light : OUT std_logic ;
      finished : OUT std_logic ;
      start_count : OUT std_logic ;
      stop_count : OUT std_logic) ;
end ctrl_oven_1hot ;

architecture impl of ctrl_oven_1hot is
   signal start_count_EXMPLR, stop_count_EXMPLR, state_6, state_4, state_5, 
      nx4, state_3, nx12, state_1, state_0, state_7, nx36, nx44, state_2, 
      nx62, nx76, nx84, nx106, nx124, nx140, nx847, nx849, nx852, nx854, 
      nx857, nx860, nx862, nx865, nx868, nx872, nx874, nx876, nx880, nx883, 
      nx886, nx890, nx892, nx893, nx896, nx899, nx901, nx903, nx905, nx912, 
      nx917: std_logic ;

begin
   start_count <= start_count_EXMPLR ;
   stop_count <= stop_count_EXMPLR ;
   ix147 : NOR21 port map ( Q=>stop_count_EXMPLR, A=>nx847, B=>nx849);
   ix848 : CLKIN1 port map ( Q=>nx847, A=>door_open);
   ix141 : NOR21 port map ( Q=>nx140, A=>door_open, B=>nx852);
   ix853 : AOI211 port map ( Q=>nx852, A=>nx854, B=>state_6, C=>
      start_count_EXMPLR);
   ix855 : CLKIN1 port map ( Q=>nx854, A=>timeout);
   reg_state_6 : DFC1 port map ( Q=>state_6, QN=>nx849, C=>clk, D=>nx140, RN
      =>nx857);
   ix858 : CLKIN1 port map ( Q=>nx857, A=>rst);
   ix131 : NOR21 port map ( Q=>start_count_EXMPLR, A=>nx860, B=>nx862);
   ix861 : CLKIN1 port map ( Q=>nx860, A=>start);
   ix125 : AOI211 port map ( Q=>nx124, A=>nx865, B=>nx868, C=>door_open);
   ix866 : NAND21 port map ( Q=>nx865, A=>nx860, B=>state_4);
   reg_state_4 : DFC1 port map ( Q=>state_4, QN=>nx862, C=>clk, D=>nx124, RN
      =>nx857);
   ix869 : AOI211 port map ( Q=>nx868, A=>time_set, B=>state_3, C=>state_5);
   ix107 : OAI221 port map ( Q=>nx106, A=>nx872, B=>nx874, C=>time_set, D=>
      nx905);
   ix873 : NOR31 port map ( Q=>nx872, A=>s60, B=>s120, C=>s30);
   ix875 : AOI221 port map ( Q=>nx874, A=>nx876, B=>state_2, C=>nx903, D=>
      state_1);
   ix877 : CLKIN1 port map ( Q=>nx876, A=>half_power);
   ix77 : OAI211 port map ( Q=>nx76, A=>half_power, B=>nx880, C=>nx901);
   ix881 : AOI221 port map ( Q=>nx880, A=>full_power, B=>nx62, C=>state_2, D
      =>nx872);
   ix63 : NAND21 port map ( Q=>nx62, A=>nx883, B=>nx893);
   reg_state_0 : DFP1 port map ( Q=>state_0, QN=>nx883, C=>clk, D=>nx44, SN
      =>nx857);
   ix45 : OAI311 port map ( Q=>nx44, A=>nx883, B=>full_power, C=>half_power, 
      D=>nx886);
   ix887 : NAND21 port map ( Q=>nx886, A=>door_open, B=>state_7);
   reg_state_7 : DFC1 port map ( Q=>state_7, QN=>nx892, C=>clk, D=>nx36, RN
      =>nx857);
   ix37 : NOR21 port map ( Q=>nx36, A=>stop_count_EXMPLR, B=>nx890);
   ix891 : AOI221 port map ( Q=>nx890, A=>timeout, B=>state_6, C=>nx847, D=>
      state_7);
   ix85 : OAI221 port map ( Q=>nx84, A=>full_power, B=>nx896, C=>nx876, D=>
      nx899);
   ix897 : AOI221 port map ( Q=>nx896, A=>half_power, B=>state_0, C=>state_1, 
      D=>nx872);
   reg_state_1 : DFC1 port map ( Q=>state_1, QN=>nx893, C=>clk, D=>nx84, RN
      =>nx857);
   reg_state_2 : DFC1 port map ( Q=>state_2, QN=>nx899, C=>clk, D=>nx76, RN
      =>nx857);
   ix902 : OAI211 port map ( Q=>nx901, A=>state_0, B=>state_1, C=>full_power
   );
   ix904 : CLKIN1 port map ( Q=>nx903, A=>full_power);
   reg_state_3 : DFC1 port map ( Q=>state_3, QN=>nx905, C=>clk, D=>nx106, RN
      =>nx857);
   reg_state_5 : DFC1 port map ( Q=>state_5, QN=>OPEN, C=>clk, D=>nx4, RN=>
      nx857);
   ix5 : AOI311 port map ( Q=>nx4, A=>nx868, B=>nx862, C=>nx849, D=>nx847);
   ix33 : OAI221 port map ( Q=>finished, A=>nx854, B=>nx849, C=>door_open, D
      =>nx892);
   ix149 : OAI2111 port map ( Q=>in_light, A=>timeout, B=>nx849, C=>nx912, D
      =>nx847);
   ix913 : NAND21 port map ( Q=>nx912, A=>start, B=>state_4);
   ix153 : OAI211 port map ( Q=>half, A=>nx876, B=>nx899, C=>nx896);
   ix71 : OAI211 port map ( Q=>full, A=>nx899, B=>nx12, C=>nx917);
   ix13 : CLKIN1 port map ( Q=>nx12, A=>nx872);
   ix918 : OAI211 port map ( Q=>nx917, A=>state_0, B=>state_1, C=>full_power
   );
end impl ;

