
-- 
-- Definition of  ctrl_oven_gray
-- 
--      Tue 08 Dec 2020 03:58:45 PM CET
--      
--      LeonardoSpectrum Level 3, 2014a.12
-- 

library c35_CORELIB;
use 	c35_CORELIB.vcomponents.all;

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity ctrl_oven_gray is
   port (
      clk : IN std_logic ;
      rst : IN std_logic ;
      half_power : IN std_logic ;
      full_power : IN std_logic ;
      start : IN std_logic ;
      s30 : IN std_logic ;
      s60 : IN std_logic ;
      s120 : IN std_logic ;
      time_set : IN std_logic ;
      door_open : IN std_logic ;
      timeout : IN std_logic ;
      full : OUT std_logic ;
      half : OUT std_logic ;
      in_light : OUT std_logic ;
      finished : OUT std_logic ;
      start_count : OUT std_logic ;
      stop_count : OUT std_logic) ;
end ctrl_oven_gray ;

architecture impl of ctrl_oven_gray is
   signal start_count_EXMPLR, state_2, state_0, state_1, nx0, nx8, nx12, 
      nx20, nx450, nx40, nx52, nx74, nx88, nx100, nx452, nx106, nx126, nx460, 
      nx462, nx465, nx467, nx469, nx472, nx476, nx479, nx481, nx487, nx490, 
      nx495, nx498, nx502, nx505, nx509, nx511, nx515, nx517, nx520, nx526: 
   std_logic ;

begin
   start_count <= start_count_EXMPLR ;
   ix139 : NOR40 port map ( Q=>stop_count, A=>nx460, B=>nx462, C=>state_1, D
      =>nx469);
   ix461 : CLKIN1 port map ( Q=>nx460, A=>door_open);
   ix107 : OAI2111 port map ( Q=>nx106, A=>nx465, B=>nx467, C=>nx502, D=>
      nx511);
   ix466 : CLKIN1 port map ( Q=>nx465, A=>half_power);
   ix127 : OAI211 port map ( Q=>nx126, A=>door_open, B=>nx469, C=>nx472);
   ix53 : NAND21 port map ( Q=>nx52, A=>nx476, B=>nx490);
   ix477 : IMUX21 port map ( Q=>nx476, A=>nx465, B=>nx40, S=>nx450);
   ix41 : OAI211 port map ( Q=>nx40, A=>nx479, B=>start_count_EXMPLR, C=>
      nx487);
   reg_state_1 : DFC1 port map ( Q=>state_1, QN=>nx479, C=>clk, D=>nx52, RN
      =>nx481);
   ix482 : CLKIN1 port map ( Q=>nx481, A=>rst);
   reg_state_0 : DFC1 port map ( Q=>state_0, QN=>nx462, C=>clk, D=>nx106, RN
      =>nx481);
   ix488 : NAND21 port map ( Q=>nx487, A=>full_power, B=>nx469);
   ix65 : NAND31 port map ( Q=>nx450, A=>state_1, B=>nx469, C=>state_0);
   ix491 : AOI311 port map ( Q=>nx490, A=>nx0, B=>door_open, C=>state_2, D=>
      nx20);
   reg_state_2 : DFC1 port map ( Q=>state_2, QN=>nx469, C=>clk, D=>nx126, RN
      =>nx481);
   ix21 : NOR40 port map ( Q=>nx20, A=>nx462, B=>state_1, C=>state_2, D=>
      nx495);
   ix496 : NOR31 port map ( Q=>nx495, A=>s60, B=>s120, C=>s30);
   ix499 : NOR21 port map ( Q=>nx498, A=>state_0, B=>state_1);
   ix503 : AOI211 port map ( Q=>nx502, A=>door_open, B=>nx452, C=>nx100);
   ix121 : OAI221 port map ( Q=>nx452, A=>nx469, B=>nx498, C=>state_0, D=>
      nx505);
   ix506 : NAND21 port map ( Q=>nx505, A=>time_set, B=>state_1);
   ix101 : OAI311 port map ( Q=>nx100, A=>timeout, B=>nx8, C=>nx469, D=>
      nx509);
   ix9 : NAND21 port map ( Q=>nx8, A=>state_0, B=>nx479);
   ix510 : NAND41 port map ( Q=>nx509, A=>start, B=>state_2, C=>state_1, D=>
      nx462);
   ix512 : AOI311 port map ( Q=>nx511, A=>nx495, B=>nx469, C=>state_0, D=>
      nx88);
   ix89 : NOR21 port map ( Q=>nx88, A=>state_1, B=>nx487);
   ix153 : OAI311 port map ( Q=>finished, A=>nx0, B=>door_open, C=>nx469, D
      =>nx515);
   ix516 : NAND21 port map ( Q=>nx515, A=>timeout, B=>nx517);
   ix518 : NOR21 port map ( Q=>nx517, A=>nx8, B=>nx469);
   ix155 : NAND21 port map ( Q=>in_light, A=>nx520, B=>nx460);
   ix169 : OAI311 port map ( Q=>half, A=>nx8, B=>state_2, C=>nx12, D=>nx526
   );
   ix13 : CLKIN1 port map ( Q=>nx12, A=>nx495);
   ix527 : NAND21 port map ( Q=>nx526, A=>half_power, B=>nx74);
   ix75 : OAI211 port map ( Q=>nx74, A=>state_2, B=>nx0, C=>nx450);
   ix185 : OAI221 port map ( Q=>full, A=>nx12, B=>nx450, C=>nx487, D=>
      state_1);
   ix473 : CLKIN1 port map ( Q=>nx472, A=>nx452);
   ix521 : CLKIN1 port map ( Q=>nx520, A=>nx100);
   ix468 : CLKIN1 port map ( Q=>nx467, A=>nx74);
   ix1 : CLKIN1 port map ( Q=>nx0, A=>nx498);
   ix35 : CLKIN1 port map ( Q=>start_count_EXMPLR, A=>nx509);
end impl ;

